/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGT1RESULTBYTESTREAM_ROIBRESULTBYTESTREAMCNV_ICC
#define TRIGT1RESULTBYTESTREAM_ROIBRESULTBYTESTREAMCNV_ICC

// Trigger includes
#include "TrigT1Result/RoIBResult.h"

// Athena includes
#include "ByteStreamCnvSvcBase/ByteStreamAddress.h"
#include "ByteStreamData/RawEvent.h"
#include "AthenaKernel/StorableConversions.h"

// Gaudi includes
#include "GaudiKernel/ConcurrencyFlags.h"

/**
 * The constructor sets up all the ToolHandle and ServiceHandle objects and initialises the
 * base class in the correct way.
 */
template< class ROBF >
RoIBResultByteStreamCnv< ROBF >::RoIBResultByteStreamCnv( ISvcLocator* svcloc )
  : Converter( storageType(), classID(), svcloc ),
    AthMessaging( msgSvc(), "RoIBResultByteStreamCnv" ),
    m_tool( "RoIBResultByteStreamTool" ),
    m_ByteStreamCnvSvc( "ByteStreamCnvSvc", "RoIBResultByteStreamCnv" ),
    m_robDataProviderSvc( "ROBDataProviderSvc", "RoIBResultByteStreamCnv" ) {

}

/**
 * Function telling the framework the Class ID of the object that this converter
 * is for (RoIBResult).
 */
template< class ROBF >
const CLID& RoIBResultByteStreamCnv< ROBF >::classID(){
  return ClassID_traits< ROIB::RoIBResult >::ID();
}

template< class ROBF >
long RoIBResultByteStreamCnv< ROBF >::storageType() {
  return ByteStreamAddress::storageType();
}

/**
 * Init method gets all necessary services etc.
 */
template< class ROBF >
StatusCode RoIBResultByteStreamCnv< ROBF >::initialize() {
  //
  // Initialise the base class:
  //
  ATH_CHECK( Converter::initialize() );
  ATH_MSG_DEBUG("In initialize()");

  //
  // Get ByteStreamCnvSvc:
  //
  ATH_CHECK( m_ByteStreamCnvSvc.retrieve() );
  ATH_MSG_DEBUG("Connected to ByteStreamCnvSvc");

  //
  // Get ROBDataProviderSvc:
  //
  ATH_CHECK( m_robDataProviderSvc.retrieve() );
  ATH_MSG_DEBUG("Connected to ROBDataProviderSvc");

  //
  // Get RoIBResultByteStreamTool:
  //
  ATH_CHECK( m_tool.retrieve() );
  ATH_MSG_DEBUG("Connected to RoIBResultByteStreamTool");

  //
  // Flag if running in AthenaMT
  //
  m_isMT = (Gaudi::Concurrency::ConcurrencyFlags::numThreads() + Gaudi::Concurrency::ConcurrencyFlags::numConcurrentEvents()) > 0;
  if (m_isMT) ATH_MSG_DEBUG("Detected running in AthenaMT");
  else ATH_MSG_DEBUG("Detected running in legacy Athena");

  return StatusCode::SUCCESS;
}

/**
 * This function creates the RoIBResult object from the BS data. It collects all the
 * ROB fragments coming from the RoI Builder and gives them to RoIBResultByteStreamTool
 * for conversion.
 */
template< class ROBF >
StatusCode RoIBResultByteStreamCnv< ROBF >::createObj( IOpaqueAddress* /*pAddr*/, DataObject*& pObj ) {
  ATH_MSG_DEBUG("createObj() called");

  // This method is deprecated in AthenaMT
  if (m_isMT) {
    ATH_MSG_ERROR("Misconfiguration! The method " << __PRETTY_FUNCTION__ << " is deprecated in multi-threaded "
                  << "mode. Please schedule RoIBResultByteStreamDecoderAlg to read RoIBResult from ByteStream.");
    return StatusCode::FAILURE;
  }

  ROIB::RoIBResult* result = new ROIB::RoIBResult;

  IROBDataProviderSvc::VROBFRAG vrobf;
  m_robDataProviderSvc->getROBData(m_tool->configuredROBIds(), vrobf, "RoIBResultByteStreamCnv");

  // Convert to Object
  ATH_CHECK( m_tool->convert( vrobf, *result ) );

  ATH_MSG_DEBUG("Conversion done");

  pObj = SG::asStorable( result ) ;
  return StatusCode::SUCCESS;

}

/**
 * This function receives an RoIBResult object as input, and adds all the ROB fragments
 * of the RoI Builder to the current raw event using the IByteStreamEventAccess
 * interface.
 */
template< class ROBF >
StatusCode RoIBResultByteStreamCnv< ROBF >::createRep( DataObject* pObj, IOpaqueAddress*& pAddr ) {
  ATH_MSG_DEBUG("createRep() called");

  RawEventWrite* re = m_ByteStreamCnvSvc->getRawEvent();

  ROIB::RoIBResult* result;

  if( ! SG::fromStorable( pObj, result ) ) {
    ATH_MSG_ERROR("Cannot cast to RoIBResult");
    return StatusCode::FAILURE;
  }

  ByteStreamAddress* addr = new ByteStreamAddress( classID(), pObj->registry()->name(), "" );

  pAddr = addr;

  return m_tool->convert( result, re );

}

#endif // TRIGT1RESULTBYTESTREAM_ROIBRESULTBYTESTREAMCNV_ICC
